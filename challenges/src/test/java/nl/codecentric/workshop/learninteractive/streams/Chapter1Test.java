package nl.codecentric.workshop.learninteractive.streams;

import nl.codecentric.workshop.learninteractive.dataProvider.SimpleDataProvider;
import nl.codecentric.workshop.learninteractive.models.Movie;
import nl.codecentric.workshop.learninteractive.testing.WorkshopRunner;
import org.apache.commons.lang3.tuple.Pair;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(WorkshopRunner.class)
public class Chapter1Test {

    Chapter1 chapter1;
    private SimpleDataProvider simpleDataProvider;

    @Before
    public void setUp() throws Exception {
        simpleDataProvider = new SimpleDataProvider();
        chapter1 = new Chapter1();
    }

    @Test
    public void challenge01() throws Exception {
        assertThat(chapter1.challenge01_StreamOfMovieIds(simpleDataProvider.getMovies()).collect(Collectors.toList()))
                .containsExactly(1L, 2L, 3L, 4L, 5L);
    }

    @Test
    public void challenge02() throws Exception {
        assertThat(chapter1.challenge02_StreamOfMovieTitles(simpleDataProvider.getMovies()).collect(Collectors.toList()))
                .containsExactly(
                        "Get out(2017)",
                        "Collide(2016)",
                        "The Shawshank Redemption (1994)",
                        "The Godfather (1972)",
                        "The Dark Knight (2008)");
    }

    @Test
    public void challenge03() throws Exception {
        assertThat(chapter1.challenge03_StreamOfFilterTitle(simpleDataProvider.getMovies()).collect(Collectors.toList()))
                .extracting(Movie::getTitle)
                .containsExactly(
                        "The Shawshank Redemption (1994)",
                        "The Godfather (1972)",
                        "The Dark Knight (2008)");
    }

    @Test
    public void challenge04() throws Exception {
        assertThat(chapter1.challenge04_StreamOfFilterId(simpleDataProvider.getMovies()).collect(Collectors.toList()))
                .extracting(Movie::getId)
                .containsExactly(2L, 4L);
    }

    @Test
    public void challenge05() throws Exception {
        final List<Movie> movies = chapter1.challenge05_StreamOfFilterId(simpleDataProvider.getMovies()).collect(Collectors
                .toList());
        assertThat(movies)
                .extracting(Movie::getId)
                .containsExactly(1L, 2L);
    }

    @Test
    public void challenge06() throws Exception {
        assertThat(chapter1.challenge06_StreamOfActionMovies(simpleDataProvider.getMovies()).collect(Collectors.toList()))
                .extracting(Movie::getId)
                .containsExactly(2L, 5L);
    }

    @Test
    public void challenge07() throws Exception {
        assertThat(chapter1.challenge07_ListOfActionMovies(simpleDataProvider.getMovies()))
                .extracting(Movie::getId)
                .containsExactly(2L, 5L);
    }

    @Test
    public void challenge08() throws Exception {
        final Map<Long, Movie> moviesMap = chapter1.challenge08_MovieById(simpleDataProvider.getMovies());
        assertThat(moviesMap).hasSize(5);
        assertThat(moviesMap.get(1L).getTitle()).isEqualTo("Get out(2017)");
        assertThat(moviesMap.get(2L).getTitle()).isEqualTo("Collide(2016)");
        assertThat(moviesMap.get(3L).getTitle()).isEqualTo("The Shawshank Redemption (1994)");
        assertThat(moviesMap.get(4L).getTitle()).isEqualTo("The Godfather (1972)");
        assertThat(moviesMap.get(5L).getTitle()).isEqualTo("The Dark Knight (2008)");
    }

    @Test
    public void challenge09() throws Exception {
        Map<Character, List<String>> movieDictionary = new HashMap<Character, List<String>>() {{
            put('G', Arrays.asList("Get out(2017)"));
            put('C', Arrays.asList("Collide(2016)"));
            put('T', Arrays.asList("The Shawshank Redemption (1994)", "The Godfather (1972)", "The Dark Knight (2008)"));
        }};

        assertThat(chapter1.challenge09_MapOfAlphabetized(simpleDataProvider.getMovies())).isEqualTo(movieDictionary);
    }

    @Test
    public void challenge10() throws Exception {
        assertThat(chapter1.challenge10_CollectionOfGenres(simpleDataProvider.getMovies())).containsExactlyInAnyOrder(
                "Action", "Horror", "Drama", "Thriller", "Crime", "Comedy", "Mystery");
    }

    @Test
    public void challenge11() throws Exception {
        assertThat(chapter1.challenge11_ListOfUniqueGenres(simpleDataProvider.getMovies())).containsExactly("Action", "Comedy",
                "Crime", "Drama", "Horror", "Mystery", "Thriller");
    }

    @Test
    public void challenge12() throws Exception {
        final Map<Long, Double> movieRatings = chapter1.challenge12_MovieRatings(simpleDataProvider.getRatings());
        AssertionsForClassTypes.assertThat(movieRatings.getOrDefault(1L, 0.0)).isEqualTo(4.5);
        AssertionsForClassTypes.assertThat(movieRatings.getOrDefault(2L, 0.0)).isEqualTo(1.0);
        AssertionsForClassTypes.assertThat(movieRatings.getOrDefault(3L, 0.0)).isEqualTo(3.0);
        AssertionsForClassTypes.assertThat(movieRatings.getOrDefault(4L, 0.0)).isEqualTo(5.0);
        AssertionsForClassTypes.assertThat(movieRatings.getOrDefault(5L, 0.0)).isEqualTo(0.0);
    }

    @Test
    public void challenge13() throws Exception {
        assertThat(chapter1.challenge13_ListOfTopMovies(simpleDataProvider.getMovies(), simpleDataProvider.getRatings()))
                .containsExactly("The Godfather (1972)");
    }

    @Test
    public void challenge14() throws Exception {
        assertThat(chapter1.challenge14_ListOfBestGenres(simpleDataProvider.getMovies(), simpleDataProvider.getRatings())).containsExactlyInAnyOrder("Horror", "Comedy", "Mystery");
    }


    @Test
    public void challenge15() throws Exception {
        final List<Pair<Integer, Double>> yearRatings = chapter1.challenge15_ListOfBestYears(simpleDataProvider.getMovies(), simpleDataProvider.getRatings());
        assertThat(yearRatings).extracting(Pair::getKey).containsExactly(1972, 2017, 1994);
        assertThat(yearRatings).extracting(Pair::getValue)
                .containsExactly(5.0, 4.5, 3.0);
    }
}
