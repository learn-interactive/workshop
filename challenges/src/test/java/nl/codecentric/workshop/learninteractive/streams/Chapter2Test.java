package nl.codecentric.workshop.learninteractive.streams;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import nl.codecentric.workshop.learninteractive.dataProvider.InfiniteDataProvider;
import nl.codecentric.workshop.learninteractive.dataProvider.MovieLensProvider;
import nl.codecentric.workshop.learninteractive.models.Movie;
import nl.codecentric.workshop.learninteractive.models.Rating;
import nl.codecentric.workshop.learninteractive.testing.WorkshopRunner;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

@RunWith(WorkshopRunner.class)
public class Chapter2Test {

    Chapter2 chapter2;
    private InfiniteDataProvider infiniteDataProvider;

    @Before
    public void setUp() throws Exception {
        infiniteDataProvider = new InfiniteDataProvider(new MovieLensProvider());
        chapter2 = new Chapter2();
    }

    @After
    public void tearDown() throws Exception {
        if (infiniteDataProvider != null) {
            infiniteDataProvider.stop();
            infiniteDataProvider = null;
        }
    }

    @Test
    public void challenge01() throws Exception {
        infiniteDataProvider.start();

        final List<String> res = new ArrayList<>();
        chapter2.challenge01_EmitMovieTitles(infiniteDataProvider.movie$(), res::add);

        Thread.sleep(1000);

        infiniteDataProvider.stop();

        assertThat(res)
                .contains(
                        "Toy Story (1995)",
                        "Jumanji (1995)",
                        "Grumpier Old Men (1995)",
                        "Waiting to Exhale (1995)",
                        "Father of the Bride Part II (1995)",
                        "Heat (1995)",
                        "Sabrina (1995)",
                        "Tom and Huck (1995)",
                        "Sudden Death (1995)",
                        "GoldenEye (1995)",
                        "American President, The (1995)",
                        "Dracula: Dead and Loving It (1995)",
                        "Balto (1995)",
                        "Nixon (1995)",
                        "Cutthroat Island (1995)",
                        "Casino (1995)",
                        "Sense and Sensibility (1995)",
                        "Four Rooms (1995)",
                        "Ace Ventura: When Nature Calls (1995)",
                        "Money Train (1995)",
                        "Get Shorty (1995)",
                        "Copycat (1995)",
                        "Assassins (1995)",
                        "Powder (1995)",
                        "Leaving Las Vegas (1995)",
                        "Othello (1995)",
                        "Now and Then (1995)",
                        "Persuasion (1995)",
                        "City of Lost Children, The (Cité des enfants perdus, La) (1995)",
                        "Shanghai Triad (Yao a yao yao dao waipo qiao) (1995)",
                        "Dangerous Minds (1995)",
                        "Twelve Monkeys (a.k.a. 12 Monkeys) (1995)",
                        "Babe (1995)",
                        "Carrington (1995)",
                        "Dead Man Walking (1995)",
                        "Across the Sea of Time (1995)",
                        "It Takes Two (1995)",
                        "Clueless (1995)",
                        "Cry, the Beloved Country (1995)",
                        "Richard III (1995)",
                        "Dead Presidents (1995)",
                        "Restoration (1995)",
                        "Mortal Kombat (1995)",
                        "To Die For (1995)",
                        "How to Make an American Quilt (1995)",
                        "Seven (a.k.a. Se7en) (1995)",
                        "Pocahontas (1995)",
                        "When Night Is Falling (1995)",
                        "Usual Suspects, The (1995)",
                        "Mighty Aphrodite (1995)",
                        "Lamerica (1994)",
                        "Big Green, The (1995)",
                        "Georgia (1995)",
                        "Home for the Holidays (1995)",
                        "Postman, The (Postino, Il) (1994)",
                        "Confessional, The (Confessionnal, Le) (1995)",
                        "Indian in the Cupboard, The (1995)",
                        "Eye for an Eye (1996)",
                        "Mr. Holland's Opus (1995)",
                        "Don't Be a Menace to South Central While Drinking Your Juice in the Hood (1996)",
                        "Two if by Sea (1996)",
                        "Bio-Dome (1996)",
                        "Lawnmower Man 2: Beyond Cyberspace (1996)",
                        "French Twist (Gazon maudit) (1995)",
                        "Friday (1995)",
                        "From Dusk Till Dawn (1996)",
                        "Fair Game (1995)",
                        "Kicking and Screaming (1995)",
                        "Misérables, Les (1995)",
                        "Bed of Roses (1996)",
                        "Screamers (1995)",
                        "Nico Icon (1995)",
                        "Crossing Guard, The (1995)",
                        "Juror, The (1996)",
                        "White Balloon, The (Badkonake sefid) (1995)",
                        "Things to Do in Denver When You're Dead (1995)",
                        "Antonia's Line (Antonia) (1995)",
                        "Once Upon a Time... When We Were Colored (1995)",
                        "Last Summer in the Hamptons (1995)",
                        "Angels and Insects (1995)",
                        "White Squall (1996)",
                        "Dunston Checks In (1996)",
                        "Black Sheep (1996)",
                        "Nick of Time (1995)",
                        "Mary Reilly (1996)",
                        "Vampire in Brooklyn (1995)",
                        "Beautiful Girls (1996)",
                        "Broken Arrow (1996)",
                        "In the Bleak Midwinter (1995)",
                        "Hate (Haine, La) (1995)",
                        "Shopping (1994)",
                        "Heidi Fleiss: Hollywood Madam (1995)",
                        "City Hall (1996)",
                        "Bottle Rocket (1996)",
                        "Mr. Wrong (1996)",
                        "Unforgettable (1996)",
                        "Happy Gilmore (1996)",
                        "Bridges of Madison County, The (1995)",
                        "Muppet Treasure Island (1996)",
                        "Catwalk (1996)",
                        "Braveheart (1995)",
                        "Taxi Driver (1976)",
                        "Rumble in the Bronx (Hont faan kui) (1995)",
                        "Before and After (1996)",
                        "Margaret's Museum (1995)",
                        "Anne Frank Remembered (1995)",
                        "Young Poisoner's Handbook, The (1995)",
                        "If Lucy Fell (1996)",
                        "Steal Big, Steal Little (1995)",
                        "Boys of St. Vincent, The (1992)",
                        "Boomerang (1992)",
                        "Chungking Express (Chung Hing sam lam) (1994)",
                        "Star Maker, The (Uomo delle stelle, L') (1995)",
                        "Flirting With Disaster (1996)",
                        "NeverEnding Story III, The (1994)",
                        "Pie in the Sky (1996)",
                        "Angela (1995)",
                        "Frankie Starlight (1995)",
                        "Jade (1995)",
                        "Down Periscope (1996)",
                        "Man of the Year (1995)",
                        "Up Close and Personal (1996)",
                        "Birdcage, The (1996)",
                        "Brothers McMullen, The (1995)",
                        "Bad Boys (1995)",
                        "Amazing Panda Adventure, The (1995)",
                        "Basketball Diaries, The (1995)",
                        "Awfully Big Adventure, An (1995)",
                        "Amateur (1994)",
                        "Apollo 13 (1995)",
                        "Rob Roy (1995)",
                        "Addiction, The (1995)",
                        "Batman Forever (1995)",
                        "Beauty of the Day (Belle de jour) (1967)",
                        "Beyond Rangoon (1995)",
                        "Blue in the Face (1995)",
                        "Canadian Bacon (1995)",
                        "Casper (1995)",
                        "Clockers (1995)",
                        "Congo (1995)",
                        "Crimson Tide (1995)",
                        "Crumb (1994)",
                        "Desperado (1995)",
                        "Devil in a Blue Dress (1995)",
                        "Die Hard: With a Vengeance (1995)",
                        "Doom Generation, The (1995)",
                        "Feast of July (1995)",
                        "First Knight (1995)",
                        "Free Willy 2: The Adventure Home (1995)",
                        "Hackers (1995)",
                        "Jeffrey (1995)",
                        "Johnny Mnemonic (1995)",
                        "Judge Dredd (1995)",
                        "Jury Duty (1995)",
                        "Kids (1995)",
                        "Living in Oblivion (1995)",
                        "Lord of Illusions (1995)",
                        "Love & Human Remains (1993)",
                        "Mad Love (1995)",
                        "Mallrats (1995)",
                        "Mighty Morphin Power Rangers: The Movie (1995)",
                        "Mute Witness (1994)",
                        "Nadja (1994)",
                        "Net, The (1995)",
                        "Nine Months (1995)",
                        "Party Girl (1995)",
                        "Prophecy, The (1995)",
                        "Reckless (1995)",
                        "Safe (1995)",
                        "Scarlet Letter, The (1995)",
                        "Showgirls (1995)",
                        "Smoke (1995)",
                        "Something to Talk About (1995)",
                        "Species (1995)",
                        "Strange Days (1995)",
                        "Umbrellas of Cherbourg, The (Parapluies de Cherbourg, Les) (1964)",
                        "Tie That Binds, The (1995)",
                        "Three Wishes (1995)",
                        "Total Eclipse (1995)"
                );
    }

    @Test
    public void challenge02() throws Exception {
        infiniteDataProvider.start();

        final List<Long> res = new ArrayList<>();
        chapter2.challenge02_EmitFirst20MovieIds(infiniteDataProvider.movie$(), res::add);

        Thread.sleep(1000);

        infiniteDataProvider.stop();

        assertThat(res)
                .containsExactly(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L,
                        11L, 12L, 13L, 14L, 15L, 16L, 17L, 18L, 19L, 20L);
    }

    @Test
    public void challenge03() throws Exception {
        infiniteDataProvider.start();

        final List<String> res = new ArrayList<>();
        chapter2.challenge03_EmitFilterTitlesStartingWithP_50Through100(infiniteDataProvider.movie$(), res::add);

        Thread.sleep(1000);

        infiniteDataProvider.stop();

        assertThat(res)
                .containsExactly(
                        "Postman, The (Postino, Il) (1994)");
    }

    @Test
    public void challenge04() throws Exception {
        infiniteDataProvider.start();

        final List<String> res = new ArrayList<>();
        chapter2.challenge04_EmitFilteredTitlesStartingWithP_50InTotal(infiniteDataProvider.movie$(), res::add);

        Thread.sleep(1000);

        infiniteDataProvider.stop();

        assertThat(res)
                .containsExactly(
                        "Powder (1995)",
                        "Persuasion (1995)",
                        "Pocahontas (1995)",
                        "Postman, The (Postino, Il) (1994)",
                        "Pie in the Sky (1996)",
                        "Party Girl (1995)",
                        "Prophecy, The (1995)",
                        "Poison Ivy II (1996)",
                        "Perez Family, The (1995)",
                        "Pyromaniac's Love Story, A (1995)",
                        "Pulp Fiction (1994)",
                        "Priest (1994)",
                        "Picture Bride (Bijo photo) (1994)",
                        "Paper, The (1994)",
                        "Perfect World, A (1993)",
                        "Philadelphia (1993)",
                        "Piano, The (1993)",
                        "Poetic Justice (1993)",
                        "Program, The (1993)",
                        "Puppet Masters, The (1994)",
                        "Pagemaster, The (1994)",
                        "Paris, France (1993)",
                        "Princess Caraboo (1994)",
                        "Pinocchio (1940)",
                        "Pretty Woman (1990)",
                        "Pallbearer, The (1996)",
                        "Primal Fear (1996)",
                        "Purple Noon (Plein soleil) (1960)",
                        "Promise, The (Versprechen, Das) (1995)",
                        "Phantom, The (1996)",
                        "Phenomenon (1996)",
                        "Phat Beach (1996)",
                        "Philadelphia Story, The (1940)",
                        "Penny Serenade (1941)",
                        "Picnic (1955)",
                        "Pompatus of Love, The (1996)",
                        "Parent Trap, The (1961)",
                        "Pollyanna (1960)",
                        "Pete's Dragon (1977)",
                        "Platoon (1986)",
                        "Palookaville (1996)",
                        "People vs. Larry Flynt, The (1996)",
                        "Perfect Candidate, A (1996)",
                        "Private Benjamin (1980)",
                        "Paths of Glory (1957)",
                        "Passion Fish (1992)",
                        "Paris Is Burning (1990)",
                        "Princess Bride, The (1987)",
                        "Psycho (1960)",
                        "Pump Up the Volume (1990)"
                );
    }

    @Test
    public void challenge05() throws Exception {
        infiniteDataProvider.start();

        final List<String> res = new ArrayList<>();
        chapter2.challenge05_EmitFilteredTitles_UntillMovieStartsWithO(infiniteDataProvider.movie$(), res::add);

        Thread.sleep(1000);

        infiniteDataProvider.stop();

        assertThat(res)
                .containsExactly(
                        "Toy Story (1995)",
                        "Jumanji (1995)",
                        "Grumpier Old Men (1995)",
                        "Waiting to Exhale (1995)",
                        "Father of the Bride Part II (1995)",
                        "Heat (1995)",
                        "Sabrina (1995)",
                        "Tom and Huck (1995)",
                        "Sudden Death (1995)",
                        "GoldenEye (1995)",
                        "American President, The (1995)",
                        "Dracula: Dead and Loving It (1995)",
                        "Balto (1995)",
                        "Nixon (1995)",
                        "Cutthroat Island (1995)",
                        "Casino (1995)",
                        "Sense and Sensibility (1995)",
                        "Four Rooms (1995)",
                        "Ace Ventura: When Nature Calls (1995)",
                        "Money Train (1995)",
                        "Get Shorty (1995)",
                        "Copycat (1995)",
                        "Assassins (1995)",
                        "Powder (1995)",
                        "Leaving Las Vegas (1995)"
                );
    }

    @Test
    public void challenge06() throws Exception {
        infiniteDataProvider.start();

        final List<Movie> res = new ArrayList<>();
        chapter2.challenge06_EmitAllActionMovies(infiniteDataProvider.movie$(), res::add);

        Thread.sleep(1000);

        infiniteDataProvider.stop();

        assertThat(res).extracting(Movie::getId)
                .containsExactly(
                        6L, 9L, 10L, 15L, 20L, 23L, 42L, 44L, 66L, 70L, 71L, 76L, 78L, 86L, 89L, 95L, 98L, 110L, 112L,
                        145L, 151L, 153L, 160L, 163L, 165L, 168L, 170L, 172L, 173L, 181L, 185L, 198L, 204L, 208L, 227L,
                        236L, 251L, 260L, 288L, 292L, 293L, 303L, 315L, 316L, 327L, 330L, 338L, 349L, 353L, 360L, 367L,
                        370L, 376L, 377L, 379L, 380L, 384L, 387L, 390L, 393L, 401L, 405L, 420L, 423L, 434L, 438L, 442L,
                        459L, 464L, 465L, 466L, 474L, 479L, 480L, 485L, 493L, 494L, 502L, 504L, 511L, 517L, 519L, 522L,
                        533L, 541L, 544L, 546L, 547L, 548L, 552L, 553L, 558L, 589L, 592L, 600L, 610L, 611L, 647L, 648L,
                        653L, 667L, 688L, 692L, 694L, 704L, 733L, 736L, 737L, 748L, 761L, 780L, 786L, 798L, 809L, 836L,
                        839L, 849L, 861L, 875L, 876L, 886L, 908L, 940L, 980L, 990L, 996L, 998L, 1004L, 1036L, 1037L,
                        1047L, 1049L, 1100L, 1101L, 1112L, 1127L, 1129L, 1168L, 1196L, 1197L, 1198L, 1200L, 1201L, 1208L,
                        1209L, 1210L, 1215L, 1218L, 1220L, 1224L, 1233L, 1240L, 1249L, 1254L, 1261L, 1262L, 1264L, 1274L,
                        1275L, 1287L, 1291L, 1304L, 1320L, 1356L, 1370L, 1372L, 1373L, 1374L, 1375L, 1377L, 1378L, 1379L,
                        1382L, 1385L, 1387L, 1389L, 1391L, 1396L, 1408L, 1427L, 1429L, 1431L, 1432L, 1438L, 1465L, 1473L,
                        1479L, 1488L, 1495L, 1497L, 1499L, 1515L, 1517L, 1518L, 1523L, 1525L, 1527L, 1544L, 1552L, 1556L,
                        1562L, 1573L, 1580L, 1586L, 1587L, 1589L, 1591L, 1599L, 1604L, 1606L, 1608L, 1610L, 1616L, 1626L,
                        1631L, 1636L, 1667L, 1676L, 1681L, 1687L, 1690L, 1722L, 1726L, 1739L, 1744L, 1752L, 1762L, 1769L,
                        1772L, 1788L, 1792L, 1796L, 1801L, 1831L, 1833L, 1858L, 1866L, 1882L, 1909L, 1917L, 1918L, 1925L,
                        1927L, 1953L, 2000L, 2001L, 2002L, 2005L, 2006L, 2013L, 2019L, 2028L, 2041L, 2048L, 2055L, 2058L,
                        2094L, 2105L, 2115L, 2126L, 2153L, 2164L, 2167L, 2170L, 2193L, 2194L, 2246L, 2273L, 2275L, 2278L,
                        2288L, 2307L, 2322L, 2334L, 2344L, 2353L, 2364L, 2365L, 2366L, 2370L, 2373L, 2376L, 2393L, 2402L,
                        2403L, 2404L, 2405L, 2406L, 2409L, 2410L, 2411L, 2412L, 2414L, 2421L, 2422L, 2427L, 2429L, 2457L,
                        2468L, 2471L, 2475L, 2476L, 2487L, 2490L, 2521L, 2524L, 2525L, 2527L, 2528L, 2529L, 2530L, 2531L,
                        2532L, 2533L, 2534L, 2535L, 2540L, 2549L, 2568L, 2571L, 2582L, 2600L, 2616L, 2617L, 2625L, 2628L,
                        2640L, 2641L, 2642L, 2643L, 2662L, 2683L, 2692L, 2701L, 2716L, 2720L, 2722L, 2723L, 2728L, 2735L,
                        2741L, 2748L, 2763L, 2802L, 2807L, 2808L, 2812L, 2815L, 2816L, 2817L, 2826L, 2835L, 2879L, 2880L,
                        2881L, 2887L, 2890L, 2892L, 2893L, 2905L, 2916L, 2924L, 2944L, 2947L, 2948L, 2949L, 2951L, 2956L,
                        2959L, 2985L, 2986L, 2989L, 2990L, 2991L, 2993L, 3000L, 3020L, 3029L, 3030L, 3032L, 3062L, 3066L,
                        3082L, 3104L, 3107L, 3113L, 3153L, 3165L, 3197L, 3204L, 3208L, 3256L, 3264L, 3265L, 3267L, 3268L,
                        3275L, 3314L, 3316L, 3384L, 3404L, 3405L, 3406L, 3430L, 3431L, 3432L, 3434L, 3438L, 3439L, 3440L,
                        3441L, 3442L, 3444L, 3452L, 3508L, 3519L, 3527L, 3555L, 3563L, 3576L, 3578L, 3584L, 3593L, 3623L,
                        3624L, 3628L, 3633L, 3635L, 3638L, 3639L, 3643L, 3654L, 3681L, 3682L, 3697L, 3698L, 3702L, 3703L,
                        3704L, 3705L, 3710L, 3716L, 3717L, 3726L, 3729L, 3740L, 3744L, 3745L, 3753L, 3761L, 3763L, 3764L,
                        3766L, 3768L, 3769L, 3771L, 3793L, 3802L, 3805L, 3827L, 3836L, 3837L, 3841L, 3864L, 3868L, 3877L,
                        3879L, 3889L, 3946L, 3947L, 3957L, 3959L, 3972L, 3977L, 3981L, 3984L, 3986L, 3996L, 3997L, 3999L,
                        4001L, 4005L, 4026L, 4042L, 4053L, 4066L, 4084L, 4085L, 4086L, 4103L, 4106L, 4121L, 4133L, 4142L,
                        4159L, 4161L, 4166L, 4180L, 4200L, 4203L, 4207L, 4210L, 4224L, 4232L, 4238L, 4262L, 4265L, 4270L,
                        4275L, 4299L, 4310L, 4339L, 4344L, 4351L, 4353L, 4355L, 4367L, 4369L, 4387L, 4396L, 4397L, 4401L,
                        4438L, 4440L, 4441L, 4442L, 4443L, 4444L, 4448L, 4466L, 4488L, 4498L, 4528L, 4531L, 4543L, 4552L,
                        4553L, 4565L, 4566L, 4567L, 4568L, 4569L, 4572L, 4614L, 4629L, 4630L, 4632L, 4636L, 4638L, 4640L,
                        4643L, 4654L, 4673L, 4675L, 4682L, 4691L, 4701L, 4717L, 4719L, 4721L, 4748L, 4749L, 4750L, 4751L,
                        4756L, 4799L, 4800L, 4802L, 4818L, 4826L, 4846L, 4855L, 4866L, 4887L, 4901L, 4915L, 4923L, 4930L,
                        4941L, 4947L, 4949L, 4956L, 4958L, 4965L, 4987L, 5001L, 5010L, 5026L, 5027L, 5039L, 5040L, 5046L,
                        5047L, 5049L, 5055L, 5064L, 5069L, 5093L, 5094L, 5152L, 5170L, 5171L, 5182L, 5219L, 5220L, 5243L,
                        5244L, 5246L, 5247L, 5248L, 5254L, 5264L, 5277L, 5300L, 5301L, 5302L, 5313L, 5349L, 5378L, 5388L,
                        5414L, 5418L, 5420L, 5438L, 5445L, 5459L, 5460L, 5463L, 5478L, 5479L, 5490L, 5507L, 5521L, 5522L,
                        5523L, 5540L, 5541L, 5558L, 5574L, 5609L, 5612L, 5621L, 5628L, 5638L, 5678L, 5691L, 5700L, 5746L,
                        5782L, 5785L, 5796L, 5803L, 5815L, 5833L, 5843L, 5872L, 5880L, 5899L, 5903L, 5915L, 5944L, 5961L,
                        5965L, 5985L, 6013L, 6014L, 6016L, 6051L, 6057L, 6059L, 6078L, 6095L, 6156L, 6157L, 6185L, 6186L,
                        6213L, 6219L, 6240L, 6261L, 6264L, 6276L, 6283L, 6294L, 6300L, 6333L, 6350L, 6365L, 6378L, 6383L,
                        6387L, 6395L, 6446L, 6448L, 6484L, 6502L, 6503L, 6506L, 6518L, 6534L, 6537L, 6539L, 6541L, 6548L,
                        6550L, 6564L, 6566L, 6582L, 6585L, 6595L, 6615L, 6618L, 6624L, 6645L, 6664L, 6686L, 6709L, 6721L,
                        6722L, 6723L, 6730L, 6754L, 6764L, 6800L, 6806L, 6808L, 6812L, 6814L, 6835L, 6857L, 6872L, 6874L,
                        6893L, 6934L, 6959L, 6966L, 6977L, 6990L, 6995L, 6996L, 7000L, 7004L, 7007L, 7016L, 7017L, 7022L,
                        7027L, 7040L, 7048L, 7056L, 7070L, 7072L, 7076L, 7090L, 7108L, 7143L, 7153L, 7163L, 7164L, 7192L,
                        7193L, 7235L, 7248L, 7272L, 7300L, 7302L, 7307L, 7309L, 7325L, 7360L, 7369L, 7373L, 7376L, 7381L,
                        7387L, 7394L, 7438L, 7439L, 7445L, 7453L, 7454L, 7458L, 7482L, 7502L, 7569L, 7570L, 7573L, 7700L,
                        7720L, 7757L, 7766L, 7782L, 7787L, 7802L, 7827L, 7844L, 7892L, 7894L, 7899L, 7915L, 7925L, 7980L,
                        7991L, 7995L, 8016L, 8093L, 8117L, 8132L, 8142L, 8167L, 8235L, 8253L, 8257L, 8268L, 8290L, 8361L,
                        8369L, 8370L, 8371L, 8403L, 8493L, 8531L, 8537L, 8576L, 8604L, 8633L, 8636L, 8640L, 8644L, 8665L,
                        8666L, 8675L, 8730L, 8795L, 8798L, 8810L, 8811L, 8832L, 8860L, 8861L, 8865L, 8908L, 8917L, 8927L,
                        8937L, 8961L, 8968L, 8972L, 8977L, 8983L, 8984L, 8985L, 25962L, 25995L, 26005L, 26012L, 26117L,
                        26152L, 26160L, 26172L, 26242L, 26322L, 26375L, 26393L, 26413L, 26425L, 26464L, 26480L, 26513L,
                        26547L, 26606L, 26614L, 26701L, 26736L, 26819L, 26840L, 26842L, 26854L, 26865L, 26886L, 27002L,
                        27022L, 27032L, 27109L, 27156L, 27193L, 27376L, 27441L, 27660L, 27689L, 27704L, 27728L, 27793L,
                        27801L, 27802L, 27816L, 27837L, 27867L, 27869L, 31101L, 31184L, 31221L, 31290L, 31420L, 31660L,
                        31696L, 31804L, 31878L, 31923L, 32017L, 32029L, 32078L, 32174L, 32211L, 32387L, 32460L, 32587L,
                        32596L, 32632L, 32898L, 33158L, 33162L, 33437L, 33493L, 33672L, 33679L, 33681L, 33794L, 33834L,
                        34048L, 34150L, 34319L, 34323L, 34332L, 34334L, 34405L, 34435L, 34520L, 34534L, 34536L, 34583L,
                        36509L, 36519L, 36529L, 36553L, 36931L, 37380L, 37386L, 37477L, 37727L, 37733L, 37830L, 37853L,
                        39052L, 39435L, 39779L, 40226L, 40278L, 40339L, 40574L, 40851L, 40959L, 41569L, 41997L, 42015L,
                        42718L, 42721L, 42738L, 42946L, 43267L, 43419L, 43558L, 43917L, 43921L, 43928L, 43932L, 44191L,
                        44245L, 44597L, 44849L, 45183L, 45186L, 45442L, 45499L, 45722L, 46322L, 46335L, 46530L, 46855L,
                        46965L, 46970L, 46972L, 47044L, 47200L, 47254L, 47330L, 47815L, 47952L, 48319L, 48522L, 48596L,
                        48711L, 48774L, 49272L, 49278L, 49314L, 49530L, 49649L, 49651L, 49688L, 50005L, 50147L, 50162L,
                        50445L, 50794L, 51077L, 51255L, 51277L, 51412L, 51662L, 51935L, 51939L, 52281L, 52287L, 52319L,
                        52460L, 52462L, 52717L, 52722L, 52831L, 52950L, 53125L, 53138L, 53464L, 53519L, 53550L, 53972L,
                        53996L, 54286L, 54648L, 54736L, 54771L, 54775L, 54995L, 54997L, 54999L, 55116L, 55167L, 55232L,
                        55247L, 55259L, 55684L, 55721L, 55995L, 56156L, 56174L, 56336L, 56775L, 56801L, 56921L, 57223L,
                        57326L, 57368L, 57528L, 57640L, 57951L, 58025L, 58103L, 58295L, 58297L, 58559L, 58627L, 59014L,
                        59037L, 59103L, 59180L, 59315L, 59369L, 59392L, 59615L, 59784L, 60040L, 60072L, 60074L, 60126L,
                        60161L, 60471L, 60514L, 60684L, 60937L, 61013L, 61024L, 61026L, 61132L, 61160L, 61210L, 61248L,
                        61289L, 61350L, 61401L, 61465L, 62081L, 62331L, 62374L, 62383L, 62394L, 62801L, 62849L, 62956L,
                        62999L, 63113L, 63276L, 63826L, 63859L, 64030L, 64508L, 64695L, 64900L, 64997L, 65514L, 65552L,
                        65682L, 65802L, 65982L, 66130L, 66297L, 66785L, 66808L, 67197L, 67252L, 67295L, 67361L, 67508L,
                        67695L, 67867L, 67923L, 68157L, 68205L, 68319L, 68358L, 68486L, 68791L, 68793L, 68945L, 68959L,
                        69275L, 69278L, 69481L, 69524L, 69526L, 69644L, 69654L, 69746L, 69805L, 69821L, 70336L, 70465L,
                        70533L, 70697L, 70728L, 71102L, 71156L, 71254L, 71460L, 71468L, 71494L, 71530L, 71533L, 71535L,
                        71573L, 71810L, 72109L, 72165L, 72171L, 72209L, 72308L, 72378L, 72489L, 72762L, 72998L, 73017L,
                        73268L, 73321L, 73323L, 73488L, 73741L, 73759L, 73929L, 74115L, 74438L, 74510L, 74532L, 74580L,
                        74668L, 74685L, 74795L, 74851L, 75805L, 75985L, 76175L, 76251L, 76293L, 77201L, 77364L, 77561L,
                        77866L, 78041L, 78105L, 78467L, 78469L, 78729L, 78893L, 79057L, 79132L, 79139L, 79185L, 79224L,
                        79274L, 79293L, 79553L, 79592L, 79695L, 79702L, 79796L, 79879L, 80026L, 80083L, 80219L, 80363L,
                        81132L, 81156L, 81229L, 81564L, 81660L, 81782L, 81834L, 82242L, 82461L, 83349L, 83374L, 83613L,
                        84187L, 84414L, 84944L, 85020L, 85056L, 85131L, 85261L, 85342L, 85401L, 85414L, 85510L, 85796L,
                        86142L, 86190L, 86332L, 86644L, 86835L, 86880L, 86892L, 86982L, 87192L, 87222L, 87232L, 87430L,
                        87520L, 87529L, 87785L, 88125L, 88140L, 88744L, 88812L, 88879L, 89087L, 89343L, 89745L, 89840L,
                        90249L, 90403L, 90524L, 90600L, 90603L, 90717L, 90738L, 90746L, 90888L, 91273L, 91470L, 91483L,
                        91485L, 91500L, 91529L, 91535L, 91542L, 91630L, 91660L, 91842L, 91974L, 91976L, 92008L, 92198L,
                        92234L, 92264L, 92420L, 92507L, 92681L, 92938L, 93326L, 93363L, 93510L, 93563L, 93766L, 93805L,
                        93838L, 94011L, 94018L, 94480L, 94777L, 94780L, 94864L, 95147L, 95165L, 95167L, 95182L, 95207L,
                        95473L, 95475L, 95499L, 95510L, 95780L, 95782L, 95875L, 95963L, 95965L, 96004L, 96007L, 96079L,
                        96417L, 96590L, 96610L, 96691L, 96737L, 96861L, 97470L, 97742L, 97757L, 97836L, 98124L, 98369L,
                        98607L, 98615L, 98829L, 98961L, 99005L, 99112L, 99114L, 99320L, 99470L, 99728L, 99813L, 100163L,
                        100304L, 100498L, 101076L, 101112L, 101362L, 101577L, 101864L, 102033L, 102123L, 102125L, 102252L,
                        102445L, 102716L, 102880L, 103042L, 103210L, 103228L, 103249L, 103253L, 103339L, 103341L, 103372L,
                        103384L, 103655L, 103659L, 103772L, 103810L, 103813L, 103883L, 104129L, 104241L, 104243L, 104312L,
                        104419L, 104760L, 104841L, 104913L, 104925L, 105121L, 105585L, 105954L, 106002L, 106072L, 106471L,
                        106473L, 106487L, 106491L, 106542L, 106762L, 106873L, 107069L, 107081L, 107314L, 107406L, 107953L,
                        108090L, 108156L, 108188L, 108689L, 108795L, 108928L, 108932L, 108945L, 108979L, 109074L, 109578L,
                        109673L, 109850L, 110102L, 110501L, 110553L, 110781L, 110826L, 111360L, 111362L, 111364L, 111659L,
                        111663L, 111759L, 111781L, 111931L, 112006L, 112138L, 112171L, 112175L, 112370L, 112818L, 112852L,
                        112897L, 112911L, 113345L, 113348L, 113416L, 113573L, 114180L, 114662L, 114795L, 114818L, 114935L,
                        115149L, 115151L, 115210L, 115617L, 115927L, 116419L, 116887L, 116939L, 117529L, 117871L, 117895L,
                        118898L, 119141L, 119145L, 120466L, 120635L, 120637L, 120783L, 120799L, 122490L, 122882L, 122886L,
                        122890L, 122892L, 122900L, 122902L, 122904L, 122920L, 122924L, 126420L, 126430L, 129657L, 129937L,
                        130083L, 130490L, 130634L, 131714L, 132046L, 132157L, 132462L, 132618L, 132796L, 133195L, 133798L,
                        134170L, 134246L, 134368L, 135137L, 135518L, 135532L, 135536L, 135567L, 135569L, 136020L, 136449L,
                        136800L, 136816L, 136864L, 138036L, 139130L, 139642L, 140711L, 143472L, 146309L, 147426L, 148168L,
                        149406L, 149612L, 150401L, 150548L, 152079L, 152081L, 156607L, 157407L, 158956L, 159093L, 159690L,
                        160080L, 160271L, 160438L, 160563L, 160565L, 161594L, 161918L, 163056L
                );
    }

    @Test
    public void challenge07() throws Exception {
        infiniteDataProvider.start();

        final List<String> res = new ArrayList<>();
        chapter2.challenge07_EmitAMovieTitleEvery10ms(infiniteDataProvider.movie$(), res::add);

        Thread.sleep(1000);

        infiniteDataProvider.stop();

        assertThat(res)
                .contains(
                        "Toy Story (1995)",
                        "Jumanji (1995)",
                        "Grumpier Old Men (1995)",
                        "Waiting to Exhale (1995)",
                        "Father of the Bride Part II (1995)",
                        "Heat (1995)",
                        "Sabrina (1995)",
                        "Tom and Huck (1995)",
                        "Sudden Death (1995)");

        if (res.size() > 9) {
            assertThat(res).contains("GoldenEye (1995)");
        } else if (res.size() > 10) {
            assertThat(res).contains("American President, The (1995)");
        } else {
            fail("Expected 10 items, and with a bit of threading leeway either 9 or 11, but we recieved " + res.size());
        }
    }

    @Test
    public void challenge08() throws Exception {

        infiniteDataProvider.start();

        final MovieLensProvider movieLensProvider = new MovieLensProvider();
        final List<Movie> movies = movieLensProvider.getMovies().subList(0, 9);
        movies.add(5, null);

        final List<Movie> resMovies = new ArrayList<>();
        final List<Throwable> resException = new ArrayList<>();

        chapter2.challenge08_CatchErrorAndProvideException(Observable.fromIterable(movies), resMovies::add, resException::add);

        Thread.sleep(1000);

        infiniteDataProvider.stop();

        assertThat(resMovies).isEqualTo(movies.subList(0, 5));
        assertThat(resException).hasSize(1);
        assertThat(resException.get(0)).isInstanceOf(NullPointerException.class);
    }

    int executerCallCount = 0;

    @Test
    public void challenge09() throws Exception {
        infiniteDataProvider.start();

        final List<Throwable> errors = new ArrayList<>();
        final List<Integer> res = new ArrayList<>();

        final Executor executor = (Runnable command) -> {
            if ((executerCallCount++) > 105) {
                final RuntimeException runtimeException = new RuntimeException("This executor should only be used to " +
                        "return the answer.");
                errors.add(runtimeException);
                throw runtimeException;
            }
            command.run();
        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                final long myThread = Thread.currentThread().getId();
                final Observable<Movie> threading$ = Observable.fromIterable(new MovieLensProvider().getMovies())
                        .map(m -> {
                            if (Thread.currentThread().getId() == myThread) {
                                final RuntimeException runtimeException = new RuntimeException("Try to offload work from this thread to another thread, using Schedulers");
                                errors.add(runtimeException);
                                throw runtimeException;
                            } else {
                                return m;
                            }
                        });

                chapter2.challenge09_EmitAllUniqueYearsOnProvidedExecutor_AndOffloadWorkFromCurrentThread(threading$, executor, res::add);
            }
        }).start();

        Thread.sleep(1000);

        infiniteDataProvider.stop();

        if (!errors.isEmpty()) {
            fail("An error ocurred during the observable chain", errors.get(0));
        }

        assertThat(res).containsExactly(
                0, 1902, 1915, 1916, 1917, 1918, 1919, 1920, 1921, 1922, 1923, 1924, 1925, 1926, 1927, 1928, 1929, 1930,
                1931, 1932, 1933, 1934, 1935, 1936, 1937, 1938, 1939, 1940, 1941, 1942, 1943, 1944, 1945, 1946, 1947,
                1948, 1949, 1950, 1951, 1952, 1953, 1954, 1955, 1956, 1957, 1958, 1959, 1960, 1961, 1962, 1963, 1964,
                1965, 1966, 1967, 1968, 1969, 1970, 1971, 1972, 1973, 1974, 1975, 1976, 1977, 1978, 1979, 1980, 1981,
                1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998,
                1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016);
        assertThat(executerCallCount).withFailMessage("Expected to receive results in the " +
                "thread associated to the executor provided as a parameter").isEqualTo(res.size() + 1);
    }

    @Test
    public void challenge10() throws Exception {

        infiniteDataProvider.start();

        final MovieLensProvider movieLensProvider = new MovieLensProvider();
        final List<Movie> movies = movieLensProvider.getMovies();
        final List<Rating> ratings = movieLensProvider.getRatings();

        final List<Movie> movieList1 = movies.subList(0, 9);
        final List<Movie> movieList2 = movies.subList(10, 19);
        final List<Movie> movieList3 = movies.subList(20, 29);

        final List<Rating> ratingSet1 = ratings.stream().filter(r -> movieList1.stream().map(m -> m.getId()).collect(Collectors.toList()).contains(r.getMovieId())).collect(Collectors.toList());
        final List<Rating> ratingSet2 = ratings.stream().filter(r -> movieList2.stream().map(m -> m.getId()).collect(Collectors.toList()).contains(r.getMovieId())).collect(Collectors.toList());
        final List<Rating> ratingSet3 = ratings.stream().filter(r -> movieList3.stream().map(m -> m.getId()).collect(Collectors.toList()).contains(r.getMovieId())).collect(Collectors.toList());

        final PublishSubject<List<Movie>> moviesList$ = PublishSubject.create();
        final PublishSubject<List<Rating>> ratingList$ = PublishSubject.create();

        final List<Throwable> errors = new ArrayList<>();
        final List<Pair<Movie, List<Rating>>> res = new ArrayList<>();
        chapter2.challenge10_LetStreamSettleAndGroupRatingsByMovie(moviesList$, ratingList$)
                .subscribe(res::add, errors::add);

        moviesList$.onNext(movieList1);
        ratingList$.onNext(ratingSet1);
        Thread.sleep(100);
        moviesList$.onNext(movieList2);
        Thread.sleep(100);
        ratingList$.onNext(ratingSet2);
        Thread.sleep(100);
        moviesList$.onNext(movieList3);
        ratingList$.onNext(ratingSet3);
        Thread.sleep(700);

        infiniteDataProvider.stop();

        if (!errors.isEmpty()) {
            fail("An exception occured during processing", errors.get(0));
        }

        assertThat(res.toString())
                .isEqualTo(
                        "[(Movie(id=21, title=Get Shorty (1995), year=1995, genres=[Comedy, Crime, Thriller]),["
                                + "Rating(userId=7, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=15, movieId=21, rating=4.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=19, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=21, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=28, movieId=21, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=30, movieId=21, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=34, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=36, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=39, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=50, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=56, movieId=21, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=57, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=60, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=66, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=73, movieId=21, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=80, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=82, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=83, movieId=21, rating=1.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=85, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=86, movieId=21, rating=1.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=88, movieId=21, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=92, movieId=21, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=93, movieId=21, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=105, movieId=21, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=113, movieId=21, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=119, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=130, movieId=21, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=135, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=144, movieId=21, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=150, movieId=21, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=151, movieId=21, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=152, movieId=21, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=161, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=165, movieId=21, rating=2.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=168, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=182, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=224, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=240, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=242, movieId=21, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=263, movieId=21, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=265, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=268, movieId=21, rating=2.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=279, movieId=21, rating=1.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=306, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=311, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=312, movieId=21, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=321, movieId=21, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=353, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=358, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=367, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=369, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=380, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=383, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=385, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=387, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=390, movieId=21, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=400, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=408, movieId=21, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=409, movieId=21, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=414, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=419, movieId=21, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=430, movieId=21, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=447, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=461, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=462, movieId=21, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=468, movieId=21, rating=2.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=472, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=491, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=508, movieId=21, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=509, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=510, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=511, movieId=21, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=514, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=516, movieId=21, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=518, movieId=21, rating=1.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=525, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=534, movieId=21, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=535, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=537, movieId=21, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=547, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=548, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=562, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=564, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=580, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=585, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=588, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=594, movieId=21, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=602, movieId=21, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=605, movieId=21, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=607, movieId=21, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=619, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=624, movieId=21, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=633, movieId=21, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=649, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=667, movieId=21, rating=3.0, dateTime=1996-06-21T13:11:33)]), (Movie(id=29, title=City of Lost Children, The (Cité des enfants perdus, La) (1995), year=1995, genres=[Adventure, Drama, Fantasy, Mystery, Sci-Fi]),["
                                + "Rating(userId=17, movieId=29, rating=4.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=19, movieId=29, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=41, movieId=29, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=56, movieId=29, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=86, movieId=29, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=90, movieId=29, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=103, movieId=29, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=119, movieId=29, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=134, movieId=29, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=148, movieId=29, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=170, movieId=29, rating=2.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=185, movieId=29, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=211, movieId=29, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=224, movieId=29, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=254, movieId=29, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=262, movieId=29, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=299, movieId=29, rating=4.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=344, movieId=29, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=346, movieId=29, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=363, movieId=29, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=383, movieId=29, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=388, movieId=29, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=396, movieId=29, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=430, movieId=29, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=450, movieId=29, rating=4.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=452, movieId=29, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=460, movieId=29, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=461, movieId=29, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=470, movieId=29, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=472, movieId=29, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=514, movieId=29, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=537, movieId=29, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=577, movieId=29, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=580, movieId=29, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=597, movieId=29, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=602, movieId=29, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=628, movieId=29, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=636, movieId=29, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=659, movieId=29, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=664, movieId=29, rating=4.0, dateTime=1996-06-21T13:11:33)]), (Movie(id=25, title=Leaving Las Vegas (1995), year=1995, genres=[Drama, Romance]),["
                                + "Rating(userId=15, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=17, movieId=25, rating=4.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=18, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=19, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=30, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=36, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=44, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=70, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=73, movieId=25, rating=4.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=80, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=86, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=87, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=90, movieId=25, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=93, movieId=25, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=100, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=102, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=105, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=111, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=113, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=128, movieId=25, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=150, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=152, movieId=25, rating=0.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=159, movieId=25, rating=4.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=161, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=182, movieId=25, rating=1.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=195, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=199, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=214, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=224, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=232, movieId=25, rating=1.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=242, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=252, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=256, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=265, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=284, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=288, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=303, movieId=25, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=304, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=306, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=312, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=313, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=328, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=329, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=330, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=338, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=342, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=343, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=345, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=361, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=373, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=380, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=381, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=383, movieId=25, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=385, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=386, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=388, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=399, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=403, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=407, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=409, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=413, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=422, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=430, movieId=25, rating=4.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=434, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=442, movieId=25, rating=1.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=447, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=459, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=461, movieId=25, rating=2.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=463, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=466, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=468, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=472, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=484, movieId=25, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=493, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=502, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=509, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=510, movieId=25, rating=1.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=511, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=514, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=516, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=527, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=529, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=533, movieId=25, rating=1.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=534, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=544, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=547, movieId=25, rating=4.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=564, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=574, movieId=25, rating=4.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=587, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=588, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=590, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=593, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=596, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=602, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=605, movieId=25, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=608, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=616, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=636, movieId=25, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=659, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=667, movieId=25, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=670, movieId=25, rating=5.0, dateTime=1996-06-21T13:11:33)]), (Movie(id=24, title=Powder (1995), year=1995, genres=[Drama, Sci-Fi]),["
                                + "Rating(userId=23, movieId=24, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=35, movieId=24, rating=2.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=95, movieId=24, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=113, movieId=24, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=119, movieId=24, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=180, movieId=24, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=185, movieId=24, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=188, movieId=24, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=207, movieId=24, rating=0.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=213, movieId=24, rating=2.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=311, movieId=24, rating=1.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=331, movieId=24, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=348, movieId=24, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=366, movieId=24, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=380, movieId=24, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=396, movieId=24, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=411, movieId=24, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=414, movieId=24, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=452, movieId=24, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=463, movieId=24, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=468, movieId=24, rating=2.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=489, movieId=24, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=514, movieId=24, rating=1.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=518, movieId=24, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=529, movieId=24, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=580, movieId=24, rating=2.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=629, movieId=24, rating=1.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=637, movieId=24, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=639, movieId=24, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=644, movieId=24, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=652, movieId=24, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=654, movieId=24, rating=4.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=656, movieId=24, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=659, movieId=24, rating=3.0, dateTime=1996-06-21T13:11:33)]), (Movie(id=26, title=Othello (1995), year=1995, genres=[Drama]),["
                                + "Rating(userId=9, movieId=26, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=70, movieId=26, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=86, movieId=26, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=119, movieId=26, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=294, movieId=26, rating=3.5, dateTime=1996-06-21T13:11:33)]), (Movie(id=22, title=Copycat (1995), year=1995, genres=[Crime, Drama, Horror, Mystery, Thriller]),["
                                + "Rating(userId=15, movieId=22, rating=2.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=19, movieId=22, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=39, movieId=22, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=66, movieId=22, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=105, movieId=22, rating=2.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=165, movieId=22, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=177, movieId=22, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=188, movieId=22, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=242, movieId=22, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=245, movieId=22, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=254, movieId=22, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=274, movieId=22, rating=2.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=282, movieId=22, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=330, movieId=22, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=380, movieId=22, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=385, movieId=22, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=388, movieId=22, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=389, movieId=22, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=409, movieId=22, rating=1.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=416, movieId=22, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=452, movieId=22, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=461, movieId=22, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=487, movieId=22, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=496, movieId=22, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=516, movieId=22, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=518, movieId=22, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=534, movieId=22, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=536, movieId=22, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=548, movieId=22, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=564, movieId=22, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=577, movieId=22, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=580, movieId=22, rating=1.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=590, movieId=22, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=608, movieId=22, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=641, movieId=22, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=652, movieId=22, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=659, movieId=22, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=664, movieId=22, rating=3.0, dateTime=1996-06-21T13:11:33)]), (Movie(id=27, title=Now and Then (1995), year=1995, genres=[Children, Drama]),["
                                + "Rating(userId=135, movieId=27, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=311, movieId=27, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=412, movieId=27, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=452, movieId=27, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=487, movieId=27, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=496, movieId=27, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=641, movieId=27, rating=4.0, dateTime=1996-06-21T13:11:33)]), (Movie(id=23, title=Assassins (1995), year=1995, genres=[Action, Crime, Thriller]),["
                                + "Rating(userId=19, movieId=23, rating=1.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=30, movieId=23, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=85, movieId=23, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=91, movieId=23, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=110, movieId=23, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=150, movieId=23, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=177, movieId=23, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=199, movieId=23, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=203, movieId=23, rating=3.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=254, movieId=23, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=286, movieId=23, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=311, movieId=23, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=344, movieId=23, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=384, movieId=23, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=388, movieId=23, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=389, movieId=23, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=496, movieId=23, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=516, movieId=23, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=534, movieId=23, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=536, movieId=23, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=548, movieId=23, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=596, movieId=23, rating=3.5, dateTime=1996-06-21T13:11:33)]), (Movie(id=28, title=Persuasion (1995), year=1995, genres=[Drama, Romance]),["
                                + "Rating(userId=67, movieId=28, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=86, movieId=28, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=99, movieId=28, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=102, movieId=28, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=122, movieId=28, rating=3.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=212, movieId=28, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=224, movieId=28, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=280, movieId=28, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=294, movieId=28, rating=4.5, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=311, movieId=28, rating=2.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=312, movieId=28, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=414, movieId=28, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=434, movieId=28, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=493, movieId=28, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=509, movieId=28, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=529, movieId=28, rating=4.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=575, movieId=28, rating=5.0, dateTime=1996-06-21T13:11:33), "
                                + "Rating(userId=614, movieId=28, rating=4.0, dateTime=1996-06-21T13:11:33)])]");
    }

    @Test
    public void challenge11() throws Exception {
        final MovieLensProvider movieLensProvider = new MovieLensProvider();
        final List<Movie> movies = movieLensProvider.getMovies();
        final List<Rating> ratings = movieLensProvider.getRatings();

        final List<Movie> movieList1 = movies.subList(0, 9);
        final List<Movie> movieList2 = movies.subList(10, 19);
        final List<Movie> movieList3 = movies.subList(20, 29);

        final List<Rating> ratingSet1 = ratings.stream().filter(r -> movieList1.stream().map(m -> m.getId()).collect(Collectors.toList()).contains(r.getMovieId())).collect(Collectors.toList());
        final List<Rating> ratingSet2 = ratings.stream().filter(r -> movieList2.stream().map(m -> m.getId()).collect(Collectors.toList()).contains(r.getMovieId())).collect(Collectors.toList());
        final List<Rating> ratingSet3 = ratings.stream().filter(r -> movieList3.stream().map(m -> m.getId()).collect(Collectors.toList()).contains(r.getMovieId())).collect(Collectors.toList());

        final PublishSubject<List<Movie>> moviesList$ = PublishSubject.create();
        final PublishSubject<List<Rating>> ratingList$ = PublishSubject.create();

        final List<Throwable> errors = new ArrayList<>();
        final List<Pair<String, Double>> res = new ArrayList<>();

        chapter2.challenge11_LetStreamSettleAndAverageRatingsByMovieTitle(moviesList$, ratingList$, res::add);

        moviesList$.onNext(movieList1);
        ratingList$.onNext(ratingSet1);
        Thread.sleep(100);
        moviesList$.onNext(movieList2);
        Thread.sleep(100);
        ratingList$.onNext(ratingSet2);
        Thread.sleep(100);
        moviesList$.onNext(movieList3);
        ratingList$.onNext(ratingSet3);
        Thread.sleep(700);

        infiniteDataProvider.stop();

        if (!errors.isEmpty()) {
            fail("An exception occured during processing", errors.get(0));
        }

        assertThat(res).extracting(Pair::getKey).containsExactlyInAnyOrder(
                "Get Shorty (1995)",
                "City of Lost Children, The (Cité des enfants perdus, La) (1995)",
                "Leaving Las Vegas (1995)",
                "Powder (1995)",
                "Othello (1995)",
                "Copycat (1995)",
                "Now and Then (1995)",
                "Assassins (1995)",
                "Persuasion (1995)"
        );

        assertThat(res).extracting(Pair::getValue).containsExactlyInAnyOrder(
                3.536842105263158,
                4.025,
                3.742574257425743,
                3.0441176470588234,
                4.1,
                3.3552631578947367,
                3.142857142857143,
                3.090909090909091,
                4.083333333333333
        );
    }
}
