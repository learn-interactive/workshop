package nl.codecentric.workshop.learninteractive.streams;

import nl.codecentric.workshop.learninteractive.models.Movie;
import nl.codecentric.workshop.learninteractive.models.Rating;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;

public class Chapter0 {

    /**
     * Mapping
     */

    // Transform the list of movies to a stream of movie Id's
    public List<Long> challenge01_StreamOfMovieIds(List<Movie> movies) {
        List<Long> ids = new ArrayList<>(movies.size());
        for (Movie movie : movies) {
            ids.add(movie.getId());
        }
        return ids;
    }

    // Transform the list of movies to a stream of movie titles
    public List<String> challenge02_StreamOfMovieTitles(List<Movie> movies) {
        List<String> titles = new ArrayList<>(movies.size());
        for (Movie movie : movies) {
            titles.add(movie.getTitle());
        }
        return titles;
    }

    /**
     * Filtering
     * @param movies
     */
    //give me all the movies which' title starts with 'The'
    public List<Movie> challenge03_StreamOfFilterTitle(List<Movie> movies) {
        List<Movie> moviesWithThe = new ArrayList<>(movies.size());
        for (Movie movie : movies) {
            if (movie.getTitle().toLowerCase().startsWith("the")) {
                moviesWithThe.add(movie);
            }
        }
        return moviesWithThe;

    }

    // Give me all the movies with an even Id
    public List<Movie> challenge04_StreamOfFilterId(List<Movie> movies) {
        List<Movie> moviesWithEvenId = new ArrayList<>(movies.size());
        for (Movie movie : movies) {
            if (movie.getId() % 2 == 0) {
                moviesWithEvenId.add(movie);
            }
        }
        return moviesWithEvenId;
    }

    // Give me all the movies with an Id below 3
    public List<Movie> challenge05_StreamOfFilterId(List<Movie> movies) {
        List<Movie> moviesWithIdBelow3 = new ArrayList<>(movies.size());
        for (Movie movie : movies) {
            if (movie.getId() < 3) {
                moviesWithIdBelow3.add(movie);
            }
        }
        return moviesWithIdBelow3;
    }

    // give me all the 'Action' movies
    public List<Movie> challenge06_StreamOfActionMovies(List<Movie> movies) {
        List<Movie> actionMovies = new ArrayList<>(movies.size());
        for (Movie movie : movies) {
            if (movie.getGenres().contains("Action")) {
                actionMovies.add(movie);
            }
        }
        return actionMovies;
    }

    /**
     * Collecting
     */
    
    // Just like the previous challenge, give me the 'Action' movies,
    // now don't return a Stream, but return a List.
    public List<Movie> challenge07_ListOfActionMovies(List<Movie> movies) {
        return challenge06_StreamOfActionMovies(movies);
    }


    // A very common pattern is to make a map from Id (movieId) to object (movie)
    // '1' -> Get out(2017),
    // '2' -> Collide(2016)
    // This is more or less the same as having an index on a database table. When we have this map,
    // we can lookup movies by their Id, in a blazingly fast manner.
    public Map<Long, Movie> challenge08_MovieById(List<Movie> movies) {
        Map<Long, Movie> movieMap = new HashMap<>();
        for (Movie movie : movies) {
            movieMap.put(movie.getId(), movie);
        }
        return movieMap;
    }

    // Give me all movies in a map, where the key is the first letter of the movies' title
    // 'l' -> La Vita e bella, Lion King,
    // 's' -> Se7en, Saving private Ryan
    // Hint: So movie titles  grouped by their first letter
    public Map<Character, List<String>> challenge09_MapOfAlphabetized(List<Movie> movies) {
        Map<Character, List<String>> movieMap = new HashMap<>();
        for (Movie movie : movies) {
            Character c = movie.getTitle().charAt(0);
            List<String> list = movieMap.get(c);
            if (list == null) {
                list = new ArrayList<>();
                movieMap.put(c, list);
            }
            list.add(movie.getTitle());
        }
        return movieMap;
    }

    // Give me all the genres, no duplicates
    // Hint: we use a Set here, because sets don't contain duplicates.
    public Set<String> challenge10_CollectionOfGenres(List<Movie> movies) {
        return getGenres(movies);
    }

    // Give me all the genres, no duplicates
    // and sorted alphabetically
    public List<String> challenge11_ListOfUniqueGenres(List<Movie> movies) {
        List<String> genres = new ArrayList<>(getGenres(movies));
        genres.sort(new Comparator<String>() {
            @Override
            public int compare(String s, String anotherString) {
                return s.compareTo(anotherString);
            }
        });
        return genres;
    }

    private Set<String> getGenres(List<Movie> movies) {
        Set<String> genres = new HashSet<>();
        for (Movie movie : movies) {
            genres.addAll(movie.getGenres());
        }

        return genres;
    }

    // Give me the average score of each movieId
    // Hint: You need the ratings collection for this. It consists of 0 or more ratings per movie.
    public Map<Long, Double> challenge12_MovieRatings(List<Movie> movies, List<Rating> ratings) {
        final Map<Movie, Double> movieRatings = getMovieRatings(movies, ratings);
        final Map<Long, Double> res = new HashMap<>();
        for (Map.Entry<Movie, Double> entry : movieRatings.entrySet()) {
            res.put(entry.getKey().getId(), entry.getValue());
        }
        return res;
    }

    // give me all the movie titles of movies rated 5 or higher (on average)
    public List<String> challenge13_ListOfTopMovies(List<Movie> movies, List<Rating> ratings) {
        List<String> res = new ArrayList<>();
        final Map<Movie, Double> movieRatings = getMovieRatings(movies, ratings);

        for (Map.Entry<Movie, Double> entry : movieRatings.entrySet()) {
            if(entry.getValue() >= 5.0) {
                res.add(entry.getKey().getTitle());
            }
        }

        return res;
    }

    //give me the top 3 highest rated genres
    public List<String> challenge14_ListOfBestGenres(List<Movie> movies, List<Rating> ratings) {
        final List<String> genres = new ArrayList<>(getGenres(movies));
        final Map<Movie, Double> movieRatings = getMovieRatings(movies, ratings);

        // Prepare the maps
        final HashMap<String, Double> ratingPerGenreMap = new HashMap<>();
        final HashMap<String, Pair<Double, Double>> aggregatesPerGenreMap = new HashMap<>();
        for (String genre : genres) {
            ratingPerGenreMap.put(genre, 0.0);
            aggregatesPerGenreMap.put(genre, Pair.of(0.0, 0.0));
        }

        // Get the aggregates for all movies with al their genres
        for (Map.Entry<Movie, Double> entry : movieRatings.entrySet()) {
            for (String genre : entry.getKey().getGenres()) {
                final Pair<Double, Double> aggregate = aggregatesPerGenreMap.get(genre);
                Double newScore = aggregate.getRight() + entry.getValue();
                aggregatesPerGenreMap.put(genre, Pair.of(aggregate.getLeft() + 1, newScore));
            }
        }

        // Collapse all the aggregates
        for (Map.Entry<String, Pair<Double, Double>> entry : aggregatesPerGenreMap.entrySet()) {
            final String genre = entry.getKey();
            final Double aggregateScore = entry.getValue().getRight();
            final Double numberOfScores = entry.getValue().getLeft();
            ratingPerGenreMap.put(genre, aggregateScore / numberOfScores);
        }

        // Sort the genres on score
        final ArrayList<Map.Entry<String, Double>> entries = new ArrayList(ratingPerGenreMap.entrySet());
        entries.sort(new Comparator<Map.Entry<String, Double>>() {
            @Override
            public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
                return (o2.getValue() - o1.getValue() > 0 ? 1 : -1);
            }
        });

        // Get the top 3 results
        final ArrayList<String> res = new ArrayList<>();
        final List<Map.Entry<String, Double>> subList = entries.subList(0, Math.min(3, ratingPerGenreMap.keySet().size()));
        for (Map.Entry<String, Double> entry : subList) {
            res.add(entry.getKey());
        }
        return res;
    }


    //give me the top 3 highest rated years, in order of score
    public List<Map.Entry<Integer, Double>> challenge15_ListOfBestYears(List<Movie> movies, List<Rating> ratings) {
        final Map<Movie, Double> movieRatings = getMovieRatings(movies, ratings);

        // Prepare the maps
        final HashMap<Integer, Double> ratingPerYearMap = new HashMap<>();
        final HashMap<Integer, Pair<Integer, Double>> aggregatesPerYearMap = new HashMap<>();
        for (Movie movie : movieRatings.keySet()) {
            ratingPerYearMap.put(movie.getYear(), 0.0);
            aggregatesPerYearMap.put(movie.getYear(), Pair.of(0, 0.0));
        }

        // Get the aggregates for all movies with al their genres
        for (Movie movie : movieRatings.keySet()) {
            final Pair<Integer, Double> aggregate = aggregatesPerYearMap.get(movie.getYear());
            Double newScore = aggregate.getRight() + movieRatings.get(movie);
            aggregatesPerYearMap.put(movie.getYear(), Pair.of(aggregate.getLeft() + 1, newScore));
        }

        // Collapse all the aggregates
        for (Map.Entry<Integer, Pair<Integer, Double>> entry : aggregatesPerYearMap.entrySet()) {
            final Integer year = entry.getKey();
            final Double aggregateScore = entry.getValue().getRight();
            final Integer numberOfScores = entry.getValue().getLeft();
            ratingPerYearMap.put(year, aggregateScore / numberOfScores);
        }

        // Sort the genres on score
        final ArrayList<Map.Entry<Integer, Double>> entries = new ArrayList(ratingPerYearMap.entrySet());
        entries.sort(new Comparator<Map.Entry<Integer, Double>>() {
            @Override
            public int compare(Map.Entry<Integer, Double> o1, Map.Entry<Integer, Double> o2) {
                return (o2.getValue() - o1.getValue() > 0 ? 1 : -1);
            }
        });

        // Get the top 3 results
        return entries.subList(0, Math.min(3, entries.size()));
    }

    private Map<Movie, Double> getMovieRatings(List<Movie> movies, List<Rating> ratings) {

        final HashMap<Long, Movie> movieMap = new HashMap<>();
        for (Movie movie : movies) {
            movieMap.put(movie.getId(), movie);
        }

        // Prepare the maps
        final HashMap<Movie, Pair<Integer, Double>> aggregatesPerMovieMap = new HashMap<>();
        final HashMap<Movie, Double> ratingPerMovieMap = new HashMap<>();
        for (Movie movie : movies) {
            ratingPerMovieMap.put(movie, 0.0);
            aggregatesPerMovieMap.put(movie, Pair.of(0, 0.0));
        }

        // Get the aggregates
        for (Rating rating : ratings) {
            final Movie movie = movieMap.get(rating.getMovieId());
            final Pair<Integer, Double> aggregate = aggregatesPerMovieMap.get(movie);
            Double newScore = aggregate.getRight() + rating.getRating();
            aggregatesPerMovieMap.put(movie, Pair.of(aggregate.getLeft() + 1, newScore));
        }

        // Collapse all the aggregates
        for (Map.Entry<Movie, Pair<Integer, Double>> entry : aggregatesPerMovieMap.entrySet()) {
            final Movie movie = entry.getKey();
            final Double aggregateScore = entry.getValue().getRight();
            final Integer numberOfScores = entry.getValue().getLeft();
            if (numberOfScores != 0.0) {
                ratingPerMovieMap.put(movie, aggregateScore / numberOfScores);
            } else {
                ratingPerMovieMap.put(movie, 0.0);
            }
        }

        return ratingPerMovieMap;
    }
}
