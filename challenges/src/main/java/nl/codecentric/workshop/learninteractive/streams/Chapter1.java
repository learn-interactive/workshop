package nl.codecentric.workshop.learninteractive.streams;

import nl.codecentric.workshop.learninteractive.models.Movie;
import nl.codecentric.workshop.learninteractive.models.Rating;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

/**
 *  Convert an iterable into a stream
 * .stream()
 *
 * Transform an item from something into something else
 * .map(item -> item.getId())
 *
 * Filter out elements which are not true
 * .filter(item -> item.isEnabled())
 *
 * Close the stream and collect everything in a list
 * .collect(Collectors.toList());
 *
 * Close the stream and collect everything in a set
 * .collect(Collectors.toSet());
 *
 * Close the stream and collect everything in a map
 * .collect(Collectors.toMap(
 *      item -> item.getId(),
 *      item -> item))
 *
 * Close the stream and collect everything in a map,
 * collecting al items that clash on the key uniqueness in a list
 * .collect(Collectors.groupingBy(
 *     item -> item.getSomethingNotUnique(),
 *      Collectors.mapping(item -> item, Collectors.toList())))
 *
 * Use as part in a collect statement, get the average of a number
 * Collectors.averagingDouble(item -> item.getCount())
 *
 * Only return unique items
 * .distinct()
 *
 * Convert a stream into anther stream
 * .flatmap(item -> item.getChildren().stream())
 *
 * Sort the items in the stream
 * .sorted(Comparator.comparingDouble(item -> item.getCount()).reversed())
 *
 * Only return the first 3 items
 * .limit(3)
 *
 * */
public class Chapter1 {

    /**
     * Mapping
     */

    // Transform the list of movies to a stream of movie Id's
    public Stream<Long> challenge01_StreamOfMovieIds(List<Movie> movies) {

        throw new NotImplementedException("");

    }

    // Transform the list of movies to a stream of movie titles
    public Stream<String> challenge02_StreamOfMovieTitles(List<Movie> movies) {

        throw new NotImplementedException("");
    }

    /**
     * Filtering
     */

    //give me all the movies which' title starts with 'The'
    public Stream<Movie> challenge03_StreamOfFilterTitle(List<Movie> movies) {

        throw new NotImplementedException("");

    }

    // Give me all the movies with an even Id
    public Stream<Movie> challenge04_StreamOfFilterId(List<Movie> movies) {

        throw new NotImplementedException("");
    }

    // Give me all the movies with an Id below 3
    public Stream<Movie> challenge05_StreamOfFilterId(List<Movie> movies) {

        throw new NotImplementedException("");
    }

    // give me all the 'Action' movies
    public Stream<Movie> challenge06_StreamOfActionMovies(List<Movie> movies) {

        throw new NotImplementedException("");
    }

    /**
     * Collecting
     */

    // Just like the previous challenge, give me the 'Action' movies,
    // now don't return a Stream, but return a List.
    public List<Movie> challenge07_ListOfActionMovies(List<Movie> movies) {

        throw new NotImplementedException("");

    }

    // A very common pattern is to make a map from Id (movieId) to object (movie)
    // '1' -> Get out(2017),
    // '2' -> Collide(2016)
    // This is more or less the same as having an index on a database table. When we have this map,
    // we can lookup movies by their Id, in a blazingly fast manner.
    public Map<Long, Movie> challenge08_MovieById(List<Movie> movies) {

        throw new NotImplementedException("");
    }

    // Give me all movies in a map, where the key is the first letter of the movies' title
    // 'l' -> La Vita e bella, Lion King,
    // 's' -> Se7en, Saving private Ryan
    // Hint: So movie titles  grouped by their first letter
    public Map<Character, List<String>> challenge09_MapOfAlphabetized(List<Movie> movies) {

        throw new NotImplementedException("");
    }

    // Give me all the genres, no duplicates.
    // Hint: we use a Set here, because sets don't contain duplicates.
    public Set<String> challenge10_CollectionOfGenres(List<Movie> movies) {

        throw new NotImplementedException("");
    }

    // Give me all the genres, no duplicates
    // and sorted alphabetically
    public List<String> challenge11_ListOfUniqueGenres(List<Movie> movies) {

        throw new NotImplementedException("");
    }

    // Give me the average score of each movieId
    // Hint: You need the ratings collection for this. It consists of 0 or more ratings per movie.
    public Map<Long, Double> challenge12_MovieRatings(List<Rating> ratings) {

        throw new NotImplementedException("");
    }

    // Give me all the movie titles of movies rated 5 or higher (on average)
    public List<String> challenge13_ListOfTopMovies(List<Movie> movies, List<Rating> ratings) {

        throw new NotImplementedException("");
    }

    // Give me the top 3 highest rated genres
    public List<String> challenge14_ListOfBestGenres(List<Movie> movies, List<Rating> ratings) {

        throw new NotImplementedException("");
    }

    // Give me the top 3 highest rated years, in order of score, including the score
    public List<Pair<Integer, Double>> challenge15_ListOfBestYears(List<Movie> movies, List<Rating> ratings) {

        throw new NotImplementedException("");
    }
}
