package nl.codecentric.workshop.learninteractive.streams;

import io.reactivex.Observable;
import nl.codecentric.workshop.learninteractive.models.Movie;
import nl.codecentric.workshop.learninteractive.models.Rating;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

/**
 * On observable that emits items async
 * movieObservable
 * <p>
 * Transform an item from something into something else
 * .map(item -> item.getId())
 * <p>
 * Start the pipeline and receive the result async
 * .subscribe(item -> ...);
 * <p>
 * Only return the first 3 items
 * .take(3)
 * <p>
 * Skip the first 3 items
 * .skip(3)
 * <p>
 * Filter out elements which are not true
 * .filter(item -> item.isEnabled())
 * <p>
 * Stop emitting new result as soon as the predicate returns false
 * .takeWhile(item -> item.isEnabled())
 * <p>
 * Delay each item for a period
 * .delay(100, TimeUnit.MILLISECONDS)
 * <p>
 * Combine two observables, using a mapping function
 * http://reactivex.io/documentation/operators/combinelatest.html
 * .combineLatest(Observable, Observable, (o1, o2) -> o1 + o2)
 * <p>
 * Only emit an item when the source stops emitting for the given period
 * .debounce(600, TimeUnit.MILLISECONDS)
 * <p>
 * Convert an observable into anther stream
 * .flatmap(item -> Observable.fromIterable(item.getChildren())
 * <p>
 * Change the thread on with the source events are emitted
 * .subscribeOn(Schedulers.computation())
 * <p>
 * Change the thread on with the statements below will be executed
 * .observeOn(Schedulers.from(executor))
 * <p>
 * Sort the items that will be emitted. Only works on non infinite source, like a .take(3) for example
 * .sorted()
 */
public class Chapter2 {

    /**
     * Transform the observable of movies to title and give them to the consumer
     */
    public void challenge01_EmitMovieTitles(
            Observable<Movie> movieObservable,
            Consumer<String> consumer) {

        throw new NotImplementedException("");
    }

    /**
     * Transform the observable of movies to ids and give the first 20 to the consumer
     */
    public void challenge02_EmitFirst20MovieIds(
            Observable<Movie> movieObservable,
            Consumer<Long> consumer) {

        throw new NotImplementedException("");
    }

    /**
     * Give all movie titles starting with a P between movies 50 and 100
     */
    public void challenge03_EmitFilterTitlesStartingWithP_50Through100(
            Observable<Movie> movieObservable,
            Consumer<String> consumer) {

        throw new NotImplementedException("");
    }

    /**
     * Give the first 50 movie titles starting with a P
     */
    public void challenge04_EmitFilteredTitlesStartingWithP_50InTotal(
            Observable<Movie> movieObservable,
            Consumer<String> consumer) {

        throw new NotImplementedException("");
    }


    /**
     * Give all movie titles until a movie starts with an O
     */
    public void challenge05_EmitFilteredTitles_UntillMovieStartsWithO(
            Observable<Movie> movieObservable,
            Consumer<String> consumer) {

        throw new NotImplementedException("");
    }

    /**
     * Give all action movies
     */
    public void challenge06_EmitAllActionMovies(
            Observable<Movie> movieObservable,
            Consumer<Movie> consumer) {

        throw new NotImplementedException("");
    }

    /**
     * Give a movie title every 10 ms
     */
    public void challenge07_EmitAMovieTitleEvery10ms(
            Observable<Movie> movieObservable,
            Consumer<String> consumer) {

        throw new NotImplementedException("");
    }

    /**
     * Return all movies, until an exception occurs, use rx's onError to catch the exception.
     * Don't null check the movies, just let the NPE happen and catch it via rx.
     */
    public void challenge08_CatchErrorAndProvideException(
            Observable<Movie> movieObservable,
            Consumer<Movie> movieConsumer,
            Consumer<Throwable> exceptionConsumer) {

        throw new NotImplementedException("");
    }


    /**
     * Provide all the unique years of all movies, in ascending order.
     * Offload all work to the computation thread, and emit all results to the consumer in the executor thread.
     */
    public void challenge09_EmitAllUniqueYearsOnProvidedExecutor_AndOffloadWorkFromCurrentThread(
            Observable<Movie> movieObservable,
            Executor executor,
            Consumer<Integer> consumer) {

        throw new NotImplementedException("");
    }

    /**
     * Wait until both movieObservable and ratingObservable stop emitting for 500 ms, then group ratings by movie and return an
     * observable
     */
    public Observable<Pair<Movie, List<Rating>>> challenge10_LetStreamSettleAndGroupRatingsByMovie(
            Observable<List<Movie>> movieObservable,
            Observable<List<Rating>> ratingObservable) {

        throw new NotImplementedException("");

    }

    /**
     * Wait until both movieObservable and ratingObservable stop emitting for 500 ms, then give the average rating by movie title
     */
    public void challenge11_LetStreamSettleAndAverageRatingsByMovieTitle(
            Observable<List<Movie>> movieObservable,
            Observable<List<Rating>> ratingObservable,
            Consumer<Pair<String, Double>> consumer) {

        throw new NotImplementedException("");
    }
}
