# Learn Interactive Workshop

Today you will learn a lot about reactive programming. 

## Prerequisites

You will need IntelliJ, and a working network connection. 
Also basic Java experience is required, since we will learn some more advances Java techniques.

## Setup

Follow these easy steps to get up and running, so that you can start the assignments. 

**Step 1: Get access**<br>
Congratulations, you already finished step 1, by obtaining access to this readme!

**Step 2: Get Git**<br>
Download the sources, we recommend using SourceTree, or the IntelliJ Git client, or commandline git. 
If you are viewing this page in Gitlab, scroll to the top and copy your personal link.

If you are familiar with Gitlab, we recommend using ssh:<br>
```git clone git@gitlab.com:learn-interactive/workshop.git```

Otherwise use https<br>
```git clone https://<your username>@gitlab.com/learn-interactive/workshop.git```

**Step 3: First Build**<br>
Open the project in IntelliJ, and select the terminal window, then execute the statement below. This will download some dependencies and might take a minute or so.<br>
```./gradlew clean build```

or

```gradlew.bat clean build``` on windows

If everything goes well you should get the following error:<br>
```nl.codecentric.workshop.learninteractive.streams.Chapter0Test > initializationError FAILED```

**Step 4: Configure IntelliJ** <br>
This step is sometimes optional, but kinda tricky, depending on you version and configuration of IntelliJ.<br>
The project uses Gradle, IntelliJ should pick this up automatically, but if this is not the case try the following:
 - Open the event log, and see if there is a message about an unlinked Gradle project. Click it.
 - Restart IntelliJ.
 - Check if there are any other messages, about missing SDKs, or Import statements.
 
**Step 5: First run from IntelliJ** <br>
Once IntelliJ is configured, run the configuration `Run Workshop Tests`. The tests will fail again, this time with errors like:<br>
```You must provide a valid Workshop ID and Username```<br>
```java.lang.IllegalStateException: FirebaseApp name [DEFAULT] already exists!```

**Step 6: Enter credentials** <br>
You should have received some credentials, update the file:
 - `challenges/src/test/resources/credentials.txt`

Uncomment the file:
 - `challenges/src/test/resources/firebase.json`

**Step 7: Great success** <br>
Now run `Run Workshop Tests` again, this time you should get some successes. 
The testsuite `Chapter0Test` should pass, and the suite `Chapter1Test` should fail.<br>

It's up to you to start on the first assignment, head to the implementation at `Chapter1.java`. Learning some streams! 

## Assignments

Once you have completed all the assignments, you will be familiar with Reactive programming, and thus the future.

### Chapter 1: Streams

In this chapter we will get familiar with Java streams, introduced in Java 8. 
Try to make the tests pass using streams. `Chapter0Test` contain solutions for the same 
testsuite, using traditional loops instead of streams.

### Chapter 2: ReactiveX

TBA

### Chapter 3: ...

TBA

### Chapter 4: ...

TBA

## FAQ

**I am trying to run the tests after the workshop, but I get error regarding my workshop ID**<br>
Your test results are automatically uploaded to firebase, so that we generate input for the later assignments.
However, we must restrict access to Firebase for obvious reasons, so if you are receiving error due to firebase, 
you could remove the `WorkshopRunner` annotation from the test classes.
